;; Copyright (C) 2010 Andreas Rottmann <a.rottmann@gmx.at>

;; This program is free software, you can redistribute it and/or
;; modify it under the terms of the new-style BSD license.

;; You should have received a copy of the BSD license along with this
;; program. If not, see <http://www.debian.org/misc/bsd.license>.

(package (wak-irregex (0 8 1) (1))
  (depends (wak-common))
  
  (synopsis "portable regular expressions")
  (description
   "A fully portable and efficient implementation of regular expressions,"
   "supporting both POSIX syntax with various (irregular) PCRE extensions,"
   "as well as SCSH's SRE syntax, with various aliases for commonly used"
   "patterns.")
  (homepage "http://synthcode.com/scheme/irregex/")
  
  (libraries
   (sls -> "wak")
   (("irregex" "private") -> ("wak" "irregex" "private"))))

;; Local Variables:
;; scheme-indent-styles: (pkg-list)
;; End:
